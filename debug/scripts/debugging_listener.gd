extends Node2D
const DEBUG_PROJ_SPEED = 1000
var projectile = load("res://scenes/projectile.tscn")
var wet = Label.new()
var burn = Label.new()
var obj = null

func _ready():
	$Debug/EntityDelta/Changes.add_child(wet)
	$Debug/EntityDelta/Changes.add_child(burn)
	

func _process(delta):
	if $Entity.effected_by_wet > 0:
		wet.text = "Effect of being Wet: " + String.num_int64($Entity.effected_by_wet)
	if $Entity.effected_by_burn > 0:
		burn.text = "Effect of being Burn(ed): " + String.num_int64($Entity.effected_by_burn)

func _on_debug_burn_entity():
	$Entity.change_state(1)


func _on_debug_neutral_entity():
	$Entity.change_state(0)


func _on_debug_wet_entity():
	$Entity.change_state(2)


func _on_debug_attach_entity(node):
	node.position = $Entity.position
	node.position.y +=100
	print($Entity.find_parent("EntityDelta"))


func _on_debug_f_projectile():
	var fire = Magic.new(Magic.ELEMENT.FIRE, 1, 0, Magic.TYPE.PROJECTILE)
	obj = projectile.instantiate()
	obj.set_effect(fire.spell)
	self.add_child(obj)
	obj.set_effect(fire.spell)
	obj.launch(obj.position.direction_to($Entity.position), DEBUG_PROJ_SPEED)
