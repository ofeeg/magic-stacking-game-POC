extends Control

signal burn_entity()
signal wet_entity()
signal neutral_entity()
signal f_projectile()

signal attach_entity(node)

func _ready():
	emit_signal("attach_entity", $EntityDelta)

	
func _on_burn_pressed():
	emit_signal("burn_entity")


func _on_wet_pressed():
	emit_signal("wet_entity")


func _on_neutral_pressed():
	emit_signal("neutral_entity")




func _on_f_proj_pressed():
	emit_signal("f_projectile")
	pass # Replace with function body.
