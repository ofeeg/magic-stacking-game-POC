extends Node
class_name Magic

enum ELEMENT{
	FIRE,
	WATER,
	}

enum TYPE {
	TARGETED,
	PROJECTILE,
	}

var spell

func _init(element, potency, m_range, type):
	spell = [element, potency, m_range, type]
	#add_user_signal("casting")
	
signal casting(target, spell)
