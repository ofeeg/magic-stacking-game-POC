extends Node

# movement junk
const MAX_SPEED = 200
var velocity = Vector2()

enum STATE {
	NEUTRAL,
	BURN,
	WET,
	}

var state = STATE.NEUTRAL

var effected_by_burn = 0
var effected_by_wet = 0
var wet_stack = 0
var stack_changed = false

func _ready():
	if not $Sprite2D:
		print("Sprite node not found")

func change_state(to_state):
	if to_state == STATE.WET:
		wet_stack += 1
		stack_changed = true
	state = to_state

func casted_on(spell):
	match spell[0]:
		Magic.ELEMENT.FIRE:
			change_state(STATE.BURN)
		Magic.ELEMENT.WATER:
			change_state(STATE.WET)
		_:
			pass
	
func state_check():
	match state:
		STATE.BURN:
			$Sprite2D.modulate = Color.RED
			effected_by_burn += 1
		STATE.WET:
			$Sprite2D.modulate = Color.BLUE
			if stack_changed:
				effected_by_wet += 255
				stack_changed = false
		_:
			$Sprite2D.modulate = Color(1,1,1)
			pass
	
func _process(delta):
	state_check()
	var movement_vector = get_movement_vector()
	var direction = movement_vector.normalized()
	velocity = direction * MAX_SPEED
	$Sprite2D.position += velocity * delta
	
func get_movement_vector():
	var x_movement = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	var y_movement = Input.get_action_strength("move_down") - Input.get_action_strength("move_up")
	
	return Vector2(x_movement, y_movement)
