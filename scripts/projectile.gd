extends StaticBody2D



var velocity = Vector2.ZERO
var effect: Array

	
func effect_resemble():
	match effect[0]:
		Magic.ELEMENT.FIRE:
			$Sprite2D.modulate = Color.RED
		Magic.ELEMENT.WATER:
			$Sprite2D.modulate = Color.BLUE
		_:
			$Sprite2D.modulate = Color(1,1,1)
			pass


func set_effect(eff):
	effect = eff
func launch(direction, speed):
	velocity = direction * speed

func _ready():
	effect_resemble()
	
func _physics_process(delta):
	move_and_collide(velocity * delta)
	


func _on_area_2d_body_entered(body):
	if !effect.is_empty() && body != self:
		body.casted_on(effect)
		self.queue_free()
