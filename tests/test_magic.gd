extends GutTest

var entity = load("res://scripts/entity.gd")
var entity_scene = load("res://scenes/entity.tscn")
var test_dummy


func _on_casting(target, spell):
	match spell[3]:
		Magic.TYPE.TARGETED:
			if target:
				gut.p(target)
		Magic.TYPE.PROJECTILE:
			var identify_state = func(num):
				match num:
					Magic.ELEMENT.FIRE:
						return "Fire " 
					Magic.ELEMENT.WATER:
						return "Water " 
					_:
						fail_test("unreachable")
			var projectile = identify_state.call(spell[0]) + " Projectile fired!\n"
			gut.p(projectile)	
		_:
			pass


func test_cast_spell():
	var fire_spell = Magic.new(Magic.ELEMENT.FIRE, 1, 0, Magic.TYPE.PROJECTILE)
	watch_signals(fire_spell)
	fire_spell.emit_signal("casting", test_dummy, fire_spell.spell)
	assert_signal_emitted(fire_spell, "casting")

func test_projectile_spell():
	var water_projectile = Magic.new(Magic.ELEMENT.WATER, 1, 0, Magic.TYPE.PROJECTILE)
	water_projectile.casting.connect(_on_casting)
	var target = Vector2(10,10)
	water_projectile.emit_signal("casting", target, water_projectile.spell)
	pass_test("Should pass")
