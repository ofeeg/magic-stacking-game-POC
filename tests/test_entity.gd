extends GutTest
var entity = load("res://scripts/entity.gd")
var entity_scene = load("res://scenes/entity.tscn")
var entities = []
const STATES = 2

func before_all():
	gut.p("Runs once before all tests")
	
func before_each():
	gut.p("Runs before each test.")
	entities.append(entity_scene.instantiate())

func after_each():
	gut.p("Runs after each test.")
	for ent in entities:
		ent.queue_free()
	
func after_all():
	gut.p("Runs once after all tests")

func test_change_state():
	entities[0].change_state(entity.STATE.BURN)
	assert_eq(entities[0].state, entity.STATE.BURN, "Should pass")

func test_effected_by_state():
	var change: String
	var identify_state = func(num, entity):
		match num:
			entity.STATE.BURN:
				return "BURN " + String.num_int64(entity.effected_by_burn)
			entity.STATE.WET:
				return "WET " + String.num_int64(entity.effected_by_wet)
			_:
				fail_test("unreachable")
	for i in range(1, STATES+1):
		entities.append(entity_scene.instantiate())
		entities[i].change_state(i) #enums translate names to numbers 1 is BURN, etc.
		gut.simulate(entities[i], 20, 0.1)
		change = "Stat effected by by " + identify_state.call(i, entities[i])
		gut.p(change)
		match i:
			entity.STATE.BURN:
				assert_gt(entities[i].effected_by_burn, 0, "Should pass")
			entity.STATE.WET:
				assert_eq(entities[i].effected_by_wet, 255, "Should pass")
			_:
				fail_test("Reached unreachable condition.")
	pass_test("All states did what was expected!")

func test_wet_stacking():
	entities[0].change_state(entity.STATE.WET)
	gut.simulate(entities[0], 1, 0.1)
	entities[0].change_state(entity.STATE.WET)
	gut.simulate(entities[0], 1, 0.1)
	assert_eq(entities[0].effected_by_wet, 255*2, "Should pass")
	
	

func test_assert_eq_number_equal():
	assert_eq('asdf', 'asdf', "Should pass")
