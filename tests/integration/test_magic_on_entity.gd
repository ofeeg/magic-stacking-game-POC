extends GutTest

var projectile = load("res://scripts/projectile.gd")
var projectile_scene = load("res://scenes/projectile.tscn")
var entity = load("res://scripts/entity.gd")
var entity_scene = load("res://scenes/entity.tscn")
var test_dummy
var SIM_DELTA = get_physics_process_delta_time()
const TEST_SPEED = 50
var obj


func _on_casting(target, spell):
	match spell[3]:
		Magic.TYPE.TARGETED:
			if target:
				target.casted_on(spell)
		Magic.TYPE.PROJECTILE:
			obj = projectile_scene.instantiate()
			self.add_child(obj)
			obj.set_effect(spell)
			obj.launch(obj.position.direction_to(target.position), TEST_SPEED)
		_:
			pass

func before_each():
	test_dummy = entity_scene.instantiate()

func after_each():
	test_dummy.queue_free()
		
func test_spell_effects():
	var fire_spell = Magic.new(Magic.ELEMENT.FIRE, 1, 0, Magic.TYPE.TARGETED)
	fire_spell.casting.connect(_on_casting)
	fire_spell.emit_signal("casting", test_dummy, fire_spell.spell)
	assert_eq(test_dummy.state, entity.STATE.BURN, "Should Pass")

	var water_spell = Magic.new(Magic.ELEMENT.WATER, 1,0,  Magic.TYPE.TARGETED)
	water_spell.casting.connect(_on_casting)
	water_spell.emit_signal("casting", test_dummy, water_spell.spell)
	assert_eq(test_dummy.state, entity.STATE.WET, "Should Pass")

#GUT testing on physics_processes is hard. Delegating this to the debug scene. You can mess with this if you like.
# The current problem is that the projectile, post-physics simulation, spawns at X:50 y:0 for a reason I don't understand. 
# func test_projectile_on_entity():
# 	self.add_child(test_dummy)
# 	test_dummy.position = Vector2(1,1)
# 	var water_projectile = Magic.new(Magic.ELEMENT.WATER, 1, 0, Magic.TYPE.PROJECTILE)
# 	water_projectile.casting.connect(_on_casting)
# 	water_projectile.emit_signal("casting", test_dummy, water_projectile.spell)
# 	gut.simulate(obj, 100, SIM_DELTA)
# 	gut.p(String.num(obj.global_position.x) + " " + String.num(obj.global_position.y) + String.num(test_dummy.position.x))#distance_to(test_dummy.position)))
# 	assert_eq(test_dummy.state, entity.STATE.WET, "Should pass")
