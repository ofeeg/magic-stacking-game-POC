extends GutTest
var projectile = load("res://scripts/projectile.gd")
var projectile_scene = load("res://scenes/projectile.tscn")
var obj
const SIM_DELTA = 0.1
const TEST_SPEED = 50

func before_each():
	obj = projectile_scene.instantiate()
	self.add_child(obj)
func after_each():
	self.remove_child(obj)
	obj.queue_free()
	
func test_launch():
	obj.launch(obj.position.move_toward(Vector2(5,5), SIM_DELTA), TEST_SPEED)
	gut.simulate(obj, 20, SIM_DELTA)
	assert_gt(obj.position, Vector2.ZERO, "Should pass")
	
